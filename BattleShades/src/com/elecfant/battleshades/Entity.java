package com.elecfant.battleshades;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.elecfant.lib.logger.Logger;

public abstract class Entity {
	
	enum CC {
		GRAY, RED, GREEN, BLUE
	}
	
	protected final static float fillPercent = 0.8f;
	protected final static float bibSize = 0.3f;
	
	protected Vector2 position;
	protected Vector2 size;
	protected float rotation;
	protected Body body;
	
	Color color;
	public Entity() {
		position = new Vector2();
		size = new Vector2();
		color = new Color();
	}
	
	public Entity(float r, float g, float b, Vector2 position, float rotation) {
		this();
		this.color.set(r, g, b, 1.0f);
		this.position.set(position);
		this.rotation = rotation;
		createBody();
		body.setUserData(this);
	}
	
	public void inflictDamage(Color color) {
		// TODO find a better solution
		this.color.sub(color);
		this.color.clamp();
	}

	public void heal(Color color) {
		// TODO find a better solution
		this.color.add(color);
		this.color.clamp();
	}
	
	public abstract void createBody();
	
	public void update(float timeElapsed) {
		this.position.set(this.body.getPosition());
		this.rotation = this.body.getAngle();
	}
	
	public void render() {
		if (body == null) {
			Logger.logMessage("Body not initialized" + this.toString(), 5);
		}

		final ShapeRenderer sr = GraphicsContext.getRenderer();
		
		sr.begin(ShapeType.Filled);
		sr.identity();
		sr.translate(this.body.getPosition().x, this.body.getPosition().y, 0);
		sr.rotate(0, 0, 1, this.body.getAngle()*MathUtils.radDeg);
		sr.setColor(Color.BLACK);
		sr.rect(-this.size.x/2, -this.size.y/2, this.size.x, this.size.y, this.size.x/2, this.size.y/2, 0);
		sr.setColor(color);
		sr.rect(-this.size.x/2*fillPercent, -this.size.y/2*fillPercent, this.size.x*fillPercent, this.size.y*fillPercent);
		sr.setColor(Color.BLACK);
		sr.rect(this.size.x*(0.5f-bibSize), -this.size.y/2*bibSize, this.size.x*bibSize, this.size.y*bibSize);
		sr.end();

		
	}

	/**
	 * @return the position
	 */
	public Vector2 getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Vector2 position) {
		this.position.set(position);
	}

	/**
	 * @return the rotation
	 */
	public float getRotation() {
		return rotation;
	}

	/**
	 * @param rotation the rotation to set
	 */
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}
	
	protected Vector2 objectToWorld(Vector2 point) {
		return this.body.getTransform().mul(point);
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}
	
	public CC getDominantColor() {
		float rb = color.r-color.b;
		float bg = color.b -color.g;
		rb *= rb;
		bg *= bg;
		
		
		if (rb + bg < 0.000032) {
			return CC.GRAY;
		}
		else if (color.r > color.b && color.r > color.g) {
			return CC.RED;
		} else if (color.b > color.g) {
			return CC.BLUE;
		} else {
			return CC.GREEN;
		}
	}
	
	protected static final float scale = 0.25f; 
	protected static final float coefMid = 1.0f*scale;
	protected static final float coefMin = 0.75f*scale;
	protected static final float coefMax = 1.25f*scale;

	public static float getColorMultiplier(CC bulletType, CC targetType) {
		if (bulletType == CC.GRAY || targetType == CC.GRAY) {
			return coefMid;
		} else if (bulletType == CC.RED){
			if (targetType == CC.GREEN) {
				return coefMax;
			}
			if (targetType == CC.BLUE){
				return coefMin;
			}
		} else if (bulletType == CC.GREEN) {
			if (targetType == CC.BLUE) {
				return coefMax;
			}
			if (targetType == CC.RED) {
				return coefMin;
			}
		} else { // bulletType == CC.BLUE
			if (targetType == CC.RED) {
				return coefMax;
			}
			if (targetType == CC.GREEN) {
				return coefMin;
			}
		}
		return coefMid;
	}

	public void dispose() {
		if (body != null) {
			body.getWorld().destroyBody(body);
		}
	}
	
	public boolean alive() {
		return !(color.r < 0.0039f && color.g < 0.0039f && color.b < 0.0039f);
	}
	
	public void rotateTowardsPoint(Vector2 point) {
		Vector2 targetVector = new Vector2(point).sub(this.body.getPosition());
		
		float targetAngle = (float)Math.atan2(targetVector.y, targetVector.x);
		
		body.setTransform(body.getPosition(),targetAngle);
	}
	
}
