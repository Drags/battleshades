package com.elecfant.battleshades;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "BattleShades";
		cfg.useGL20 = true;
		cfg.vSyncEnabled = true;
		cfg.width = (int)(1366*0.75f);
		cfg.height = (int)(768*0.75f);
		
		new LwjglApplication(new BattleShadesMain(), cfg);
	}
}
