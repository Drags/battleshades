package com.elecfant.battleshades;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Input {

	protected static Rectangle leftStick;
	protected static Rectangle rightStick;

	public static void init() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();

		float shorterDim = h < w ? h : w;

		float stickSize = shorterDim * 0.5f;

		leftStick = new Rectangle(0, 0, stickSize, stickSize);
		rightStick = new Rectangle(w - stickSize, 0, stickSize, stickSize);
		System.out.println("ls " + leftStick);
		System.out.println("rs " + rightStick);
	}

	public static Vector2 getLeftStick() {
		if (Gdx.app.getType() == ApplicationType.Android) {
			for (int i = 0; i < 2; ++i) {
				Vector2 screenSpace = new Vector2(Gdx.input.getX(i), Gdx.input.getY(i));
				if (leftStick.contains(screenSpace)) {
					Vector2 center = new Vector2();
					leftStick.getCenter(center);
					screenSpace.sub(center);
					screenSpace.scl(1.0f / (leftStick.width / 2));
					screenSpace.y *= -1.0f;
					if (screenSpace.len2() > 1.0) {
						screenSpace.nor();
					}
					return screenSpace;
				}
			}
		}
		return new Vector2();
	}

	public static Vector2 getRightStick() {
		if (Gdx.app.getType() == ApplicationType.Android) {
			for (int i = 0; i < 2; ++i) {
				Vector2 screenSpace = new Vector2(Gdx.input.getX(i), Gdx.input.getY(i));
				if (rightStick.contains(screenSpace)) {
					Vector2 center = new Vector2();
					rightStick.getCenter(center);
					screenSpace.sub(center);
					screenSpace.scl(1.0f / (rightStick.width / 2));
					screenSpace.y *= -1.0f;
					if (screenSpace.len2() > 1.0) {
						screenSpace.nor();
					}
					return screenSpace;
				}
			}
		}
		return new Vector2();
	}

	public static void render() {
		if (Gdx.app.getType() == ApplicationType.Android) {
			final ShapeRenderer sr = GraphicsContext.getRenderer();
			sr.begin(ShapeType.Filled);
			sr.identity();
			sr.setColor(new Color(0, 0, 0, 0.5f));
			// sr.rect(0,0, 10, 10, 0, 0, 0);
			sr.translate(-Gdx.graphics.getWidth() / 2, -Gdx.graphics.getHeight() / 2, 0);
			sr.rect(leftStick.x, leftStick.y, leftStick.width, leftStick.height);
			sr.rect(rightStick.x, rightStick.y, rightStick.width, rightStick.height);
			sr.end();
		}
	}
}
