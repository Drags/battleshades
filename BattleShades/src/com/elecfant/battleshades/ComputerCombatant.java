package com.elecfant.battleshades;

import com.badlogic.gdx.math.Vector2;

public class ComputerCombatant extends Combatant {

	public ComputerCombatant(float r, float g, float b, Vector2 position, float rotation) {
		super(r, g, b, position, rotation);
	}
	
	@Override
	public void update(float timeElapsed) {
		super.update(timeElapsed);
		Entity target = Level.getClosestEntity(this, this.body.getPosition(), Combatant.class);
		Vector2 vector = new Vector2(target.getPosition()).sub(this.position);
		if (vector.len2() > 5*5) {
			vector.nor().scl(speed);
			this.body.applyLinearImpulse(vector, this.body.getWorldPoint(new Vector2(0.0f,0.0f)), true);
		} else {
			if (weaponFired <= 0) {
				Level.addEntity(this.generateBullet());
			}
		}
		rotateTowardsPoint(target.getPosition());
	}

}
