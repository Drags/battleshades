package com.elecfant.battleshades;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.elecfant.lib.logger.Logger;

public class BattleShadesMain implements ApplicationListener {
	
	final static float timeStep = 1.0f/60.0f;
	static boolean debugToggle = false;
	protected Box2DDebugRenderer debugRenderer;
	
	PlayerCombatant player;
	@Override
	public void create() {	
		
		Logger.init();

		Input.init();
		debugRenderer = new Box2DDebugRenderer(true, true, false, true, true, true);
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		//Audio.playsound();
		player = new PlayerCombatant(0.5f, 0.5f, 0.5f, new Vector2(0,0), 0);
		Level.addEntity(player);
		//Level.addEntity(new Doodle(0.3f, 0.3f, 0.3f, new Vector2(2,2),0));
		//Level.addEntity(new Doodle(0.0f, 1.0f, 0.3f, new Vector2(-2,2),0));
		//Level.addEntity(new Doodle(0.0f, 1.0f, 0.0f, new Vector2(-2,-2),0));
		
		float size = 30;
		float halfsize = size/2;
		int enemyQty = 5;
		int doodleQty = 20;
		
		for (int i = 0; i < doodleQty; ++i) {
			Level.addEntity(new Doodle((float)Math.random(), (float)Math.random(), (float)Math.random(),
					new Vector2((float)(-halfsize+size*Math.random()),(float)(-halfsize+size*Math.random())),
					0));
		}
		for (int i = 0; i < enemyQty; ++i) {
			Level.addEntity(new ComputerCombatant((float)Math.random(), (float)Math.random(), (float)Math.random(),
					new Vector2((float)(-halfsize+size*Math.random()),(float)(-halfsize+size*Math.random())),
					0));
		}
		
	}

	@Override
	public void dispose() {

	}

	@Override
	public void render() {		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		
		Level.getWorld().step(timeStep, 2, 7);
		
		Level.update(timeStep);
		GraphicsContext.getCamera().position.x = player.getPosition().x;
		GraphicsContext.getCamera().position.y = player.getPosition().y;
		GraphicsContext.getCamera().update();
		GraphicsContext.getRenderer().setProjectionMatrix(GraphicsContext.getCamera().combined);
		Level.render();
		if (Gdx.input.isKeyPressed(Keys.ALT_LEFT) && Gdx.input.isKeyPressed(Keys.F) && Gdx.input.isKeyPressed(Keys.Q)) {
			debugToggle ^= true;
		}
		if (debugToggle) {
			debugRenderer.render(Level.getWorld(), GraphicsContext.getCamera().combined);
		}
		GraphicsContext.getRenderer().setProjectionMatrix(GraphicsContext.getGUICamera().projection);
		Input.render();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
