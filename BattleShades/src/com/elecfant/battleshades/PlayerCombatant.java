package com.elecfant.battleshades;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;

public class PlayerCombatant extends Combatant {

	public PlayerCombatant(float r, float g, float b, Vector2 position, float rotation) {
		super(r, g, b, position, rotation);
	}
	/* (non-Javadoc)
	 * @see com.elecfant.battleshades.Entity#update(float)
	 */
	@Override
	public void update(float timeElapsed) {
		super.update(timeElapsed);

		Vector2 centerInWorld = this.body.getWorldPoint(new Vector2(0.0f,0.0f));
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			rotateTowardsPoint(GraphicsContext.unproject(Gdx.input.getX(), Gdx.input.getY()));
			
			if (Gdx.input.isKeyPressed(Keys.A)) {
				this.body.applyLinearImpulse(new Vector2(-speed,0), centerInWorld, true);
			}
			if (Gdx.input.isKeyPressed(Keys.D)) {
				this.body.applyLinearImpulse(new Vector2(speed,0), centerInWorld, true);
			}
			if (Gdx.input.isKeyPressed(Keys.W)){
				this.body.applyLinearImpulse(new Vector2(0,speed), centerInWorld, true);
			}
			if (Gdx.input.isKeyPressed(Keys.S)) {
				this.body.applyLinearImpulse(new Vector2(0,-speed), centerInWorld, true);
			}
			if (Gdx.input.isKeyPressed(Keys.SPACE) && weaponFired <= 0) {
				Level.addEntity(this.generateBullet());
			}
		} else if (Gdx.app.getType() == ApplicationType.Android) {
			Vector2 left = Input.getLeftStick();
			Vector2 right = Input.getRightStick();
			System.out.println(left + " and " + right == null ? "null":right);
			if (right != null) {
				float targetAngle = (float)Math.atan2(right.y, right.x);
				body.setTransform(body.getPosition(),targetAngle);
				if (weaponFired <= 0) {
					Level.addEntity(this.generateBullet());
				}
			}
			this.body.applyLinearImpulse(left.scl(speed),centerInWorld, true);
		}
	}
}
