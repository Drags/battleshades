package com.elecfant.battleshades;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Doodle extends Entity {

	
	
	public Doodle(float r, float g, float b, Vector2 position, float rotation) {
		super(r, g, b, position, rotation);
		this.size.set(1,1);
	}

	@Override
	public void createBody() {
		final BodyDef bd = new BodyDef();
		bd.type = BodyType.StaticBody;
		bd.position.set(this.position);
		bd.angle = this.rotation;
		body = Level.getWorld().createBody(bd);
		final FixtureDef fd = new FixtureDef();
		fd.density = 1.0f;
		final PolygonShape ps = new PolygonShape();
		ps.setAsBox(0.5f, 0.5f);
		fd.shape = ps;
		body.createFixture(fd);
		ps.dispose();
	}

}
