package com.elecfant.battleshades;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Bullet extends Entity {

	protected Combatant shooter;
	
	protected float speed = 75.0f;

	public Bullet(float r, float g, float b, Vector2 position, float rotation) {
		super(r, g, b, position, rotation);
	}

	public Bullet(float r, float g, float b, Vector2 position, float rotation, Combatant shooter) {
		this(r, g, b, position, rotation);
		this.shooter = shooter;
		this.size.set(0.5f, 0.25f);
		this.body.applyForce(new Vector2(speed+shooter.body.getLinearVelocity().len(), 0).rotate(this.body.getAngle() * MathUtils.radDeg), this.objectToWorld(new Vector2()), true);
	}

	public void setShooter(Combatant shooter) {
		this.shooter = shooter;
	}

	public void detonate(Entity target) {
		if (target != null) {
			CC thisType = this.getDominantColor();
			
			CC targetType = target.getDominantColor();
			float multiplier = Entity.getColorMultiplier(thisType, targetType);
			Color damage = new Color(this.color);
			damage.mul(target.color);
			damage.mul(multiplier);
			System.out.println("Attacker: " + thisType + " defender: " + targetType + " total damage: " + damage.r + " " + damage.g + " " +damage.b);
			target.inflictDamage(damage);
			this.shooter.heal(damage);
		}
		this.color.set(0, 0, 0, 0);
	}

	@Override
	public void createBody() {
		final BodyDef bd = new BodyDef();
		bd.type = BodyType.DynamicBody;
		bd.position.set(this.position);
		bd.angle = this.rotation;
		body = Level.getWorld().createBody(bd);
		final FixtureDef fd = new FixtureDef();
		fd.density = 1.0f;
		final PolygonShape ps = new PolygonShape();
		ps.setAsBox(0.25f, 0.125f);
		fd.shape = ps;
		body.createFixture(fd);
		ps.dispose();
	}

}
