package com.elecfant.battleshades;

import java.util.ListIterator;
import java.util.Vector;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;
import com.elecfant.lib.logger.Logger;

public class Level {
	protected static World world;
	protected static Vector<Entity> entities;
	protected static Vector<Entity> newEntities;

	public static World getWorld() {
		if (world == null) {
			Logger.logMessage("Box2D World was not initialized, initializing...", 5);
			init();
		}
		return world;
	}

	public static void addEntity(Entity entity) {
		if (entities == null) {
			Logger.logMessage("Entity vector was not initialized, intializing...", 5);
			init();
		}
		newEntities.add(entity);
	}

	public static void init() {
		world = new World(new Vector2(0, 0), true);
		world.setContactListener(new ContactListener() {

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub

			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub

			}

			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beginContact(Contact contact) {
				Entity first = (Entity) contact.getFixtureA().getBody().getUserData();
				Entity second = (Entity) contact.getFixtureB().getBody().getUserData();
				if (first instanceof Bullet && second instanceof Bullet) {
					((Bullet)first).detonate(null);
					((Bullet)second).detonate(null);
				}
				if (first instanceof Bullet) {
					((Bullet)first).detonate(second);
				} else if (second instanceof Bullet) {
					((Bullet)second).detonate(first);
				}

			}
		});
		
		entities = new Vector<Entity>();
		newEntities = new Vector<Entity>();
	}

	public static void update(float timeElapsed) {
		if (newEntities.size() > 0) {
			entities.addAll(newEntities);
			newEntities.clear();
		}
		Entity current = null;
		ListIterator<Entity> it = entities.listIterator();
		while (it.hasNext()) {
			current = it.next();
			if (current.alive()) {
				current.update(timeElapsed);
			} else {
				current.dispose();
				it.remove();
			}
		}
	}

	public static void render() {
		for (Entity entity : entities) {
			entity.render();
		}
		
	}

	public static Entity getClosestEntity(Entity caller, Vector2 position, Class<? extends Entity> limitClass) {
		float distance = Float.MAX_VALUE;
		Entity result = null;
		for (Entity entity : entities) {
			if (entity == caller) {
				continue;
			}
			
			if (limitClass == null || limitClass.isInstance(entity)) {
				final float tempDist = caller.getPosition().dst(entity.getPosition());
				if ( tempDist < distance) {
					distance = tempDist;
					result = entity;
				}
			}
		}
		return result;
	}
}
