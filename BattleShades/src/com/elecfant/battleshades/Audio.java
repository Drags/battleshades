package com.elecfant.battleshades;

import javax.sound.midi.Instrument;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Synthesizer;

public class Audio {

	static Synthesizer s = null;
	
	public static void playsound() {
		
		try {
			s = MidiSystem.getSynthesizer();
		} catch (MidiUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Instrument[] inst = s.getAvailableInstruments();
		System.out.println("Available midi instruments: "+inst.length);
		for (int i = 0; i < inst.length; ++i) {
			System.out.println(inst[i].getName());
		}
	}
}
