package com.elecfant.battleshades;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Combatant extends Entity {
	protected static float cooldown = 0.5f;
	
	protected float weaponFired = 0.0f;
	
	protected float speed = 0.25f;
	public Combatant(float r, float g, float b, Vector2 position, float rotation) {
		super(r, g, b, position, rotation);
		this.size.set(1,1);
	}

	public Bullet generateBullet() {
		weaponFired = cooldown;
		return new Bullet(this.color.r, this.color.g, this.color.b, this.objectToWorld(new Vector2(1, 0)), this.body.getAngle(), this);
	}

	@Override
	public void createBody() {
		final BodyDef bd = new BodyDef();
		bd.type = BodyType.DynamicBody;
		bd.position.set(this.position);
		bd.angle = this.rotation;
		body = Level.getWorld().createBody(bd);
		final FixtureDef fd = new FixtureDef();
		fd.density = 1.0f;
		final PolygonShape ps = new PolygonShape();
		ps.setAsBox(0.5f, 0.5f);
		fd.shape = ps;
		body.createFixture(fd);
		ps.dispose();
	}
	
	@Override
	public void update(float timeElapsed) {
		super.update(timeElapsed);
		Vector2 speed = new Vector2(this.body.getLinearVelocity());
		speed.scl(-0.1f);
		this.body.applyLinearImpulse(speed, this.objectToWorld(new Vector2()), true);
		if (weaponFired > 0) {
			weaponFired -= timeElapsed;
		}
	}
	
}
