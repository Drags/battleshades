package com.elecfant.battleshades;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class GraphicsContext {
	protected static ShapeRenderer sr;

	protected static OrthographicCamera camera;
	protected static OrthographicCamera guiCamera;
	
	public static ShapeRenderer getRenderer() {
		if (sr == null) {
			sr = new ShapeRenderer();
		}
		return sr;
	}
	public static Camera getCamera() {
		if (camera == null) {
			float w = Gdx.graphics.getWidth();
			float h = Gdx.graphics.getHeight();
			camera = new OrthographicCamera(w, h);
			camera.zoom = 0.025f;
			camera.update();
		}
		return camera;
	}
	
	public static Camera getGUICamera() {
		if (guiCamera == null) {
			float w = Gdx.graphics.getWidth();
			float h = Gdx.graphics.getHeight();
			
			guiCamera = new OrthographicCamera(w, h);
			guiCamera.setToOrtho(false);
			guiCamera.update();
		}
		return guiCamera;
	}
	public static Vector2 unproject(int x, int y) {
		getCamera();
		
		Vector3 mousePosScreen = new Vector3(x, y, 0.0f);
		camera.unproject(mousePosScreen);
		return new Vector2(mousePosScreen.x, mousePosScreen.y);//.scl(camera.zoom);
	}
}
